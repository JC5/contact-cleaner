<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cards extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'cards', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();

            // basic info:
            $table->boolean('local_update')->default(0);
            $table->dateTime('last_modified');
            $table->string('uid', 255);
            $table->string('version', 10)->nullable();
            $table->string('etag', 255);

            $table->date('dod')->nullable();
            $table->date('anniversary')->nullable();
            $table->date('birthday')->nullable();
            $table->string('nickname', 255)->nullable();


            $table->mediumText('notes')->nullable();
            $table->string('photo_type', 10)->nullable();
            $table->string('photo_sha2', 64)->nullable();
            $table->string('photo_uri', 255)->nullable();
            $table->longText('photo')->nullable();
        }
        );
    }
}
