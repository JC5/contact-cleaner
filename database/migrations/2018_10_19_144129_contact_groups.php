<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactGroups extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cards_groups');
        Schema::dropIfExists('contact_groups');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'contact_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('group', 255);
        }
        );

        if (!Schema::hasTable('cards_groups')) {
            Schema::create(
                'cards_groups',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('card_id', false, true);
                    $table->integer('contact_group_id', false, true);

                    $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
                    $table->foreign('contact_group_id')->references('id')->on('contact_groups')->onDelete('cascade');

                    // unique combi:
                    $table->unique(['card_id', 'contact_group_id']);
                }
            );
        }
    }
}
