<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactAddresses extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_types');
        Schema::dropIfExists('contact_addresses');
        Schema::dropIfExists('contact_address_types');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'contact_address_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('type', 15);
        }
        );

        Schema::create(
            'contact_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('card_id', false, true);

            $table->string('po_box', 255);
            $table->string('extended_address', 255);
            $table->string('street_address', 255);
            $table->string('postal_code', 255);
            $table->string('locality', 255);
            $table->string('region', 255);
            $table->string('country', 255);

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');

        }
        );


        if (!Schema::hasTable('address_types')) {
            Schema::create(
                'address_types',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('contact_address_id', false, true);
                    $table->integer('contact_address_type_id', false, true);

                    $table->foreign('contact_address_id')->references('id')->on('contact_addresses')->onDelete('cascade');
                    $table->foreign('contact_address_type_id')->references('id')->on('contact_address_types')->onDelete('cascade');

                    // unique combi:
                    $table->unique(['contact_address_id', 'contact_address_type_id']);
                }
            );
        }
    }
}
