<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactOrgs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_orgs', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('card_id', false, true);

            $table->string('name', 255);
            $table->string('unit1', 255)->nullable();
            $table->string('unit2', 255)->nullable();

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_orgs');
    }
}
