<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactEmails extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_types');
        Schema::dropIfExists('contact_emails');
        Schema::dropIfExists('contact_email_types');
        
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'contact_email_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('type', 15);
        }
        );
        
        Schema::create(
            'contact_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('card_id', false, true);
            $table->string('email', 255);

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');

        }
        );

        if (!Schema::hasTable('email_types')) {
            Schema::create(
                'email_types',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('contact_email_id', false, true);
                    $table->integer('contact_email_type_id', false, true);

                    $table->foreign('contact_email_id')->references('id')->on('contact_emails')->onDelete('cascade');
                    $table->foreign('contact_email_type_id')->references('id')->on('contact_email_types')->onDelete('cascade');

                    // unique combi:
                    $table->unique(['contact_email_id', 'contact_email_type_id']);
                }
            );
        }
    }
}
