<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactNames extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_names');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'contact_names', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('card_id', false, true);
            // name info:
            $table->string('full_name', 255)->nullable();
            $table->string('last_name', 255)->nullable();
            $table->string('first_name', 255)->nullable();
            $table->string('additional_names', 255)->nullable();
            $table->string('prefixes', 255)->nullable();
            $table->string('suffixes', 255)->nullable();

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
        }
        );
    }
}
