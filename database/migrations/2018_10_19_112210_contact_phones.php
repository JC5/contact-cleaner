<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContactPhones extends Migration
{
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phone_types');
        Schema::dropIfExists('contact_phones');
        Schema::dropIfExists('contact_phone_types');

    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create(
            'contact_phone_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('type', 15);
        }
        );

        Schema::create(
            'contact_phones', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('card_id', false, true);
            $table->string('number', 50);

            $table->foreign('card_id')->references('id')->on('cards')->onDelete('cascade');
        }
        );

        if (!Schema::hasTable('phone_types')) {
            Schema::create(
                'phone_types',
                function (Blueprint $table) {
                    $table->increments('id');
                    $table->integer('contact_phone_id', false, true);
                    $table->integer('contact_phone_type_id', false, true);

                    $table->foreign('contact_phone_id')->references('id')->on('contact_phones')->onDelete('cascade');
                    $table->foreign('contact_phone_type_id')->references('id')->on('contact_phone_types')->onDelete('cascade');

                    // unique combi:
                    $table->unique(['contact_phone_id', 'contact_phone_type_id']);
                }
            );
        }
    }
}
