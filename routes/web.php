<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses' => 'HomeController@home', 'as' => 'home']);

// photo stuff
Route::get('photo/display/{card}', ['uses' => 'PhotoController@display', 'as' => 'photo.display']);

// contact stuff
Route::get('contact/view/{card}', ['uses' => 'ContactController@view', 'as' => 'contact.view']);

// scanner stuff
Route::get('scanner', ['uses' => 'ScanController@index', 'as' => 'scanner.index']);
Route::get('scanner/fix', ['uses' => 'ScanController@fix', 'as' => 'scanner.fix']);

// information lists
Route::get('information/emailDomains', ['uses' => 'InformationController@listEmailDomains', 'as' => 'information.list_email_domains']);