# NextCloud Contact Cleaner

This is a basic tool that can connect to a CardDAV server (such as NextCloud) to clean up contact details and push them back to the server. Examples include:

* Company logo as a profile picture based on their work address.
* Correct classification
* Alerts for missing data.

The tool tries to fix this and push it back to the server.

*Use at your own risk and always make a backup*