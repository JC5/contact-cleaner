<?php
declare(strict_types=1);

use ContactCleaner\Contact\Fixer\BadPhoneType;
use ContactCleaner\Contact\Fixer\EmailClassification;
use ContactCleaner\Contact\Fixer\GroupByDomain;
use ContactCleaner\Contact\Fixer\IncompleteAddress;
use ContactCleaner\Contact\Fixer\Name;
use ContactCleaner\Contact\Fixer\NoDetails;
use ContactCleaner\Contact\Fixer\NoName;
use ContactCleaner\Contact\Fixer\PhoneNumberFormat;
use ContactCleaner\Contact\Fixer\PhotoByEmailDomain;
use ContactCleaner\Contact\Fixer\PhotoByGroup;
use ContactCleaner\Contact\Fixer\Spaces;
use ContactCleaner\Contact\Fixer\DoubleContacts;

return [
    'fixers' => [
        GroupByDomain::class,
        PhotoByGroup::class,
        NoDetails::class,
        PhoneNumberFormat::class,
        NoName::class,
        EmailClassification::class,
        Spaces::class,
        Name::class,
        PhotoByEmailDomain::class,
        BadPhoneType::class,
        IncompleteAddress::class,
    ],
    'global_fixers' => [
        DoubleContacts::class,
    ],

    'last_name_classification' => [
        'Dorigo' => 'Familie',
    ],
    // email classification:
    'email_classification'     => [
        'abnamro.com'     => 'work',
        'luminus.eu'      => 'work',
        'luminis.eu'      => 'work',
        'ordina.nl'       => 'work',
        'rabobank.nl'     => 'work',
        'aeteurope.nl'    => 'work',
        'lopexs.com'      => 'work',
        'ibm.com'         => 'work',
        'primekey.com'    => 'work',
        'infocaster.net'  => 'work',
        'infosupport.com' => 'work',
        'thesio.eu'       => 'work',
        'carthago-ict.nl' => 'work',
        'cgi.com'         => 'work',
        'thalesgroup.com' => 'work',
        'ru.nl'           => 'work',
        'gmail.com'       => 'home',
        'hotmail.com'     => 'home',
        'dorigo.nl'       => 'home',
        'gmx.net'         => 'home',
        'live.nl'         => 'home',
        'me.com'          => 'home',
        'ziggo.nl'        => 'home',
        'upcmail.nl'      => 'home',
        'xs4all.nl'       => 'home',
        'kpnmail.nl'      => 'home',
        'nder.be'         => 'home',
        'avensus.nl'      => 'work',
        'keytalk.com'     => 'work',
    ],


    // domains and the groups they're expected to have.
    'domains_and_groups'       => [
        'abnamro.com'     => 'ABN AMRO',
        'luminus.eu'      => 'Luminis',
        'luminis.eu'      => 'Luminis',
        'ordina.nl'       => 'Ordina',
        'rabobank.nl'     => 'Rabobank',
        'aeteurope.nl'    => 'Bedrijven',
        'primekey.com'    => 'Bedrijven',
        'lopexs.com'      => 'Luminis',
        'dorigo.nl'       => 'Familie',
        'ibm.com'         => 'IBM',
        'infocaster.net'  => 'Bedrijven',
        'infosupport.com' => 'Bedrijven',
        'thesio.eu'       => 'Bedrijven',
        'carthago-ict.nl' => 'Bedrijven',
        'cgi.com'         => 'Bedrijven',
        'thalesgroup.com' => 'Bedrijven',
        'ru.nl'           => 'Radboud',
        'avensus.nl'      => 'Bedrijven',
    ],
    'groups_and_photos'        => [
        'ABN AMRO' => 'https://i.nder.be/images/2018/10/21/abn-amro.png',
        'Rabobank' => 'https://i.nder.be/images/2018/10/21/rabobank.png',
        'Luminis'  => 'https://i.nder.be/images/2018/10/21/luminis.png',
        'Ordina'   => 'https://i.nder.be/images/2018/10/21/ordina.png',
        'IBM'      => 'https://i.nder.be/images/2018/11/11/640px-IBM_logo.svg.png',
    ],
    'domains_and_photos'       => [
        'primekey.com' => 'https://i.nder.be/images/2018/11/23/primekey.png',
        'keytalk.com'  => 'https://i.nder.be/images/2018/11/24/keytalk.png',
    ],
];