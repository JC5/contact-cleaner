<?php

namespace ContactCleaner\Http\Controllers;

use ContactCleaner\CardDAV\CardDAV;
use ContactCleaner\Contact\Fixer\ScannerInterface;
use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactAddress;
use ContactCleaner\Models\ContactEmail;
use ContactCleaner\Models\ContactName;
use ContactCleaner\Models\ContactOrg;
use ContactCleaner\Models\ContactPhone;
use Illuminate\Support\Facades\Log;
use JeroenDesloovere\VCard\VCard;

class ScanController extends Controller
{
    /**
     *
     */
    public function fix()
    {
        // grab cards with local updates:
        $cards = Card::with(
            [
                'contactNames', 'contactOrgs',
                'contactPhones', 'contactPhones.contactPhoneTypes',
                'contactAddresses', 'contactAddresses.contactAddressTypes',
                'contactEmails', 'contactEmails.contactEmailTypes',
                'contactGroups']
        )->where('local_update', 1)->take(5)->get();

        Log::debug(sprintf('Looping %d cards', $cards->count()));

        /** @var Card $card */
        foreach ($cards as $index => $card) {
            //$card  = Card::where('local_update', 1)->first();
            //$index = 0;

//            Log::debug(sprintf('Now at card #%d', $index));
//            // make a new vCard.
//            $vcard = new VCard();
//
//            // fill in name:
//            /** @var ContactName $dbName */
//            $dbName = $card->contactNames->first();
//            $vcard->addName($dbName->last_name, $dbName->first_name, $dbName->additional_names, $dbName->prefixes, $dbName->suffixes);
//
//            // add addresses:
//            /** @var ContactAddress $address */
//            foreach ($card->contactAddresses as $address) {
//                $type = strtoupper(implode(';', $address->contactAddressTypes->pluck('type')->toArray()));
//                $vcard->addAddress(
//                    $address->po_box, $address->extended_address, $address->street_address, $address->locality, $address->region, $address->postal_code,
//                    $address->country,
//                    $type
//                );
//            }
//
//            // add emails:
//            /** @var ContactEmail $email */
//            foreach ($card->contactEmails as $email) {
//                $type = strtoupper(implode(';', $email->contactEmailTypes->pluck('type')->toArray()));
//                $vcard->addEmail($email->email, $type);
//            }
//
//            // add groups:
//            $groups = $card->contactGroups->pluck('group')->toArray();
//            if (\count($groups) > 0) {
//                $vcard->addCategories($groups);
//            }
//            // organisations:
//            /** @var ContactOrg $dbOrg */
//            $dbOrg = $card->contactOrgs->first();
//            if (null !== $dbOrg) {
//                $vcard->addCompany($dbOrg->name);
//                $vcard->addRole($dbOrg->unit1);
//            }
//
//            // add phone numbers
//            /** @var ContactPhone $phone */
//            foreach ($card->contactPhones as $phone) {
//                $type = strtoupper(implode(';', $phone->contactPhoneTypes->pluck('type')->toArray()));
//                $vcard->addPhoneNumber($phone->number, $type);
//            }
//
//            // add photo:
//            if (null !== $card->photo) {
//                $vcard->addPhotoContent(base64_decode($card->photo));
//            }
//
//            $output = $vcard->getOutput();
//
//            Log::debug(sprintf('Output is %s', $output));
//
//            // connect to card dav server
//            $carddav = new CardDAV('https://cloud.sanderdorigo.nl/remote.php/dav/addressbooks/users/Sander/contacts/');
//            $carddav->set_auth('Sander', '62Fa9-kpPqR-ALAnD-egzPA-Bciea');
//            try {
//                $carddav->update($output, $card->uid);
//            } catch (\Exception $e) {
//                Log::error('Error: ' . $e->getMessage());
//            }
//            //            $card->local_update = 0;
//            //            $card->save();
//            echo 'Updated ' . $dbName->full_name . ':<br>';
//            //echo '<pre>' . $output . '</pre><hr>';
//            $card->local_update = 0;
//            $card->save();

            //var_dump($card->toArray());
        }
    }

    /**
     *
     */
    public function index()
    {

        $cards    = Card::with(['contactEmails', 'contactNames', 'contactGroups'])->get();
        $scanners = config('cc.scanners');
        $problems = [];

        Log::debug(sprintf('Have %d cards', $cards->count()));
        Log::debug(sprintf('Have %d scanners', \count($scanners)));

        foreach ($scanners as $scanner) {
            Log::debug(sprintf('Now at %s', $scanner));

            /** @var ScannerInterface $class */
            $class = app($scanner);
            /** @var Card $card */
            foreach ($cards as $card) {
                /** @var ContactName $name */
                $name = $card->contactNames->first();
                Log::debug(sprintf('Now at card #%d ("%s")', $card->id, $name->full_name));
                $class->setCard($card);
                $results = $class->scan();
                /** @noinspection SlowArrayOperationsInLoopInspection */
                $problems = array_merge($problems, $results);
                Log::debug(sprintf('Total number of problems is now %d.', \count($problems)));
            }
        }


        return view('scanner.index', compact('problems'));
    }

}
