<?php

namespace ContactCleaner\Http\Controllers;

use ContactCleaner\Models\Card;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function home()
    {
        $cards = Card::with(['ContactNames'])->get()->toArray();


        return view('home', compact('cards'));
    }
}
