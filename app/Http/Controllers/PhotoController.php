<?php

namespace ContactCleaner\Http\Controllers;

use ContactCleaner\Models\Card;

class PhotoController extends Controller
{
    public function display(Card $card)
    {
        $photoData = null;
        $mimes     = [
            'png' => 'image/png',
        ];
        $mime      = 'image/png';
        if (null === $card->photo) {
            $photoData = file_get_contents(resource_path('contact-images/no-photo.png'));
        }

        if (null !== $card->photo) {

            $photoData = base64_decode($card->photo);
            $mime      = $mimes[$card->photo_type] ?? 'image/png';
        }

        return response()->make(
            $photoData, 200, [
                          'Content-Type' => $mime,
                          'Expires'      => 'Sat, 26 Jul 2020 05:00:00 GMT',
                      ]
        );
    }
}
