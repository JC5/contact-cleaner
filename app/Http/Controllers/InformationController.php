<?php
declare(strict_types=1);

namespace ContactCleaner\Http\Controllers;


use ContactCleaner\Models\ContactEmail;

class InformationController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function listEmailDomains()
    {
        $emails  = ContactEmail::get(['email']);
        $grouped = [];
        /** @var ContactEmail $email */
        foreach ($emails as $email) {
            $address        = $email->email;
            $parts          = explode('@', $address);
            $host           = $parts[1] ?? '(empty)';
            $grouped[$host] = isset($grouped[$host]) ? $grouped[$host] + 1 : 1;
        }
        arsort($grouped);

        return view('information.list_email_domains',compact('grouped'));
    }

}