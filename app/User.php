<?php

namespace ContactCleaner;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * ContactCleaner\User
 *
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\User whereUpdatedAt($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
