<?php
declare(strict_types=1);

namespace ContactCleaner\Support;

use Carbon\Carbon;
use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactAddress;
use ContactCleaner\Models\ContactAddressType;
use ContactCleaner\Models\ContactEmail;
use ContactCleaner\Models\ContactEmailType;
use ContactCleaner\Models\ContactEvent;
use ContactCleaner\Models\ContactGroup;
use ContactCleaner\Models\ContactName;
use ContactCleaner\Models\ContactOrg;
use ContactCleaner\Models\ContactPhone;
use ContactCleaner\Models\ContactPhoneType;
use ContactCleaner\VCard\VCardExtended;
use ContactCleaner\VCard\VCardParser;

/**
 *
 * Class CardSync
 */
class CardSync
{
    /**
     * @param Card $card
     *
     * @return string
     */
    public function generateVCard(Card $card): string
    {
        // make a new vCard.
        $vcard = new VCardExtended();
        //
        // fill in name:
        /** @var ContactName $dbName */
        $dbName = $card->contactNames->first();
        if (null !== $dbName) {
            $vcard->addName($dbName->last_name, $dbName->first_name, $dbName->additional_names, $dbName->prefixes, $dbName->suffixes);
        }
        // add addresses:
        /** @var ContactAddress $address */
        foreach ($card->contactAddresses as $address) {
            $type = strtoupper(implode(';', $address->contactAddressTypes->pluck('type')->toArray()));
            $vcard->addAddress(
                $address->po_box, $address->extended_address, $address->street_address, $address->locality, $address->region, $address->postal_code,
                $address->country,
                $type
            );
        }

        // add emails:
        /** @var ContactEmail $email */
        foreach ($card->contactEmails as $email) {
            $type = strtoupper(implode(';', $email->contactEmailTypes->pluck('type')->toArray()));
            $vcard->addEmail($email->email, $type);
        }

        // add groups:
        $groups = $card->contactGroups->pluck('group')->toArray();
        if (\count($groups) > 0) {
            $vcard->addCategories($groups);
        }
        // organisations:
        /** @var ContactOrg $dbOrg */
        $dbOrg = $card->contactOrgs->first();
        if (null !== $dbOrg) {
            $vcard->addCompany($dbOrg->name);
            $vcard->addRole($dbOrg->unit1);
        }

        // add phone numbers
        /** @var ContactPhone $phone */
        foreach ($card->contactPhones as $phone) {
            $type = strtoupper(implode(';', $phone->contactPhoneTypes->pluck('type')->toArray()));

            $number = str_replace(',', '\,', $phone->number);

            $vcard->addPhoneNumber($number, $type);
        }

        $eventIndex = 1;

        // add birthday
        if (null !== $card->birthday) {
            $vcard->addBirthday($card->birthday->format('Y-m-d'));
        }
        // add date of death
        if (null !== $card->dod) {
            $vcard->addDateOfDeath($card->dod->format('Y-m-d'));

            // also add DoD as custom event?
            $vcard->addEvent($eventIndex, $card->dod->format('Y-m-d'), 'Overlijden');
            $eventIndex++;
        }

        // add anniversary:
        if (null !== $card->anniversary) {
            $vcard->addAnniversary($card->anniversary->format('Y-m-d'));

            // also add as custom event. Assume it's a trouwdag
            $vcard->addEvent($eventIndex, $card->anniversary->format('Y-m-d'), 'Anniversary');
            $eventIndex++;
        }

        // add photo:
        if (null !== $card->photo_uri) {
            $vcard->addPhotoUri($card->photo_uri);
        }
        if ('' !== (string)$card->notes) {
            $vcard->addNote($card->notes);
        }

        return $vcard->getOutput();

    }

    /**
     * @param Card   $card
     * @param string $vCardText
     *
     * @param string $etag
     *
     * @return Card
     */
    public function updateDatabase(Card $card, string $vCardText, string $etag): Card
    {
        // parse the vCard text:
        $parsed = new VCardParser(false, $vCardText);

        // update the card again:
        $card->version      = $parsed->version[0] ?? '';
        $card->etag         = $etag;
        $card->local_update = false;
        $card->save();

        // store the name field(s) in the database.
        // delete old name fields first:
        $card->contactNames()->delete();
        foreach ($parsed->n as $index => $name) {
            $contactName = new ContactName;
            $contactName->card()->associate($card);
            $contactName->full_name        = $parsed->fn[$index] ?? '';
            $contactName->first_name       = $name['FirstName'] ?? '';
            $contactName->last_name        = $name['LastName'] ?? '';
            $contactName->additional_names = $name['AdditionalNames'] ?? '';
            $contactName->prefixes         = $name['Prefixes'] ?? '';
            $contactName->suffixes         = $name['Suffixes'] ?? '';
            $contactName->save();
        }

        // loop and save the phone number(s)
        $card->contactPhones()->delete();
        foreach ($parsed->tel as $index => $number) {

            if (\is_array($number)) {
                $contactPhone = new ContactPhone;
                $contactPhone->card()->associate($card);
                $contactPhone->number = $number['Value'];
                $contactPhone->save();

                foreach ($number['Type'] as $type) {
                    $phoneType = ContactPhoneType::where('type', $type)->first();
                    if (null === $phoneType) {
                        $phoneType       = new ContactPhoneType;
                        $phoneType->type = $type;
                        $phoneType->save();
                    }
                    $contactPhone->contactPhoneTypes()->save($phoneType);
                }
            }
            if (!\is_array($number)) {
                $contactPhone = new ContactPhone;
                $contactPhone->card()->associate($card);
                $contactPhone->number = $number;
                $contactPhone->save();
            }
        }

        // loop and save the email(s)
        $card->contactEmails()->delete();
        foreach ($parsed->email as $index => $mail) {
            $contactEmail = new ContactEmail;
            $contactEmail->card()->associate($card);
            $contactEmail->email = $mail['Value'];
            $contactEmail->save();

            foreach ($mail['Type'] as $type) {
                $emailType = ContactEmailType::where('type', $type)->first();
                if (null === $emailType) {
                    $emailType       = new ContactEmailType;
                    $emailType->type = $type;
                    $emailType->save();
                }
                $contactEmail->contactEmailTypes()->save($emailType);
            }
        }

        // loop and save the organisation(s)
        $card->contactOrgs()->delete();
        foreach ($parsed->org as $index => $org) {
            $contactOrg = new ContactOrg;
            $contactOrg->card()->associate($card);
            $contactOrg->name  = $org['Name'];
            $contactOrg->unit1 = $org['Unit1'];
            $contactOrg->unit2 = $org['Unit2'];
            $contactOrg->save();
        }


        // DoD:
        if (isset($parsed->deathdate[0]) && 8 === \strlen($parsed->deathdate[0])) {
            $card->dod = Carbon::createFromFormat('Ymd', $parsed->deathdate[0]);
            $card->save();

            // create separate event:
            $date = Carbon::parse($parsed->deathdate[0]);
            $dateStr = $date->format('Y-m-d');
            $label = 'Overlijden';
            $cardId = $card->id;
            ContactEvent::firstOrCreate(['date' => $dateStr, 'label' => $label, 'card_id' => $cardId]);

        }
        // DoD different format:
        if (isset($parsed->deathdate[0]) && 10 === \strlen($parsed->deathdate[0])) {
            $card->dod = Carbon::createFromFormat('Y-m-d', $parsed->deathdate[0]);
            $card->save();

            // create separate event:
            $date = Carbon::parse($parsed->deathdate[0]);
            $dateStr = $date->format('Y-m-d');
            $label = 'Overlijden';
            $cardId = $card->id;
            ContactEvent::firstOrCreate(['date' => $dateStr, 'label' => $label, 'card_id' => $cardId]);
        }

        // anniversary (of what?)
        if (isset($parsed->anniversary[0])) {
            $card->anniversary = Carbon::parse($parsed->anniversary[0]);
            $card->save();

            // create separate event:
            $date = Carbon::parse($parsed->anniversary[0]);
            $dateStr = $date->format('Y-m-d');
            $label = 'Anniversary';
            $cardId = $card->id;
            ContactEvent::firstOrCreate(['date' => $dateStr, 'label' => $label, 'card_id' => $cardId]);
        }
        // nickname
        if (isset($parsed->nickname[0][0])) {
            $card->nickname = (string)$parsed->nickname[0][0];
            $card->save();
        }

        // birthday
        $bDay = $parsed->bday;
        if (isset($bDay[0])) {
            $birthday = $bDay[0];
            $saved    = false;

            if (6 === \strlen($birthday)) {
                if (0 === strpos($birthday, '--')) {
                    $birthday = str_replace('--', '1900', $birthday);
                }
                $card->birthday = Carbon::parse($birthday);
                $saved          = true;
            }

            if (\strlen($birthday) === 8) {
                $card->birthday = Carbon::createFromFormat('Ymd', $birthday);
                $saved          = true;
            }
            if (\strlen($birthday) === 10) {
                $card->birthday = Carbon::createFromFormat('Y-m-d', $birthday);
                $saved          = true;
            }
            if (\strlen($birthday) > 10 && 'T' === $birthday{10}) {
                $card->birthday = Carbon::createFromFormat('Y-m-d', substr($birthday, 0, 10));
                // make sure we trigger a local update to fix this.
                $card->local_update = true;
                $saved              = true;
            }
            if (\strlen($birthday) > 10 && 'T' === $birthday{8}) {
                $card->birthday = Carbon::createFromFormat('Ymd', substr($birthday, 0, 8));
                // make sure we trigger a local update to fix this.
                $card->local_update = true;
                $saved              = true;
            }

            if (false === $saved) {
                die(sprintf('Cannot handle birthday %s', $birthday));
            }

            $card->save();
        }

        // save the notes
        $notes       = $parsed->note[0] ?? null;
        $card->notes = $notes;
        $card->save();

        // loop and save the photos

        if (is_array($parsed->photo) && count($parsed->photo)) {
            // save the photo:
            $url = $parsed->photo[0];
            if (is_string($url)) {
                $card->photo_uri = $url;
                $card->save();
            }
            if (!is_string($url)) {
                $card->photo_type = $photo['Type'][0] ?? null;
                $card->photo      = $photo['Value'] ?? null;
                $card->photo_sha2 = hash('sha256', $photo['Value'] ?? 'empty');
                $card->save();
            }
        }

        // addresses
        $card->contactAddresses()->delete();
        foreach ($parsed->adr as $index => $address) {

            $contactAddress = new ContactAddress;
            $contactAddress->card()->associate($card);

            $contactAddress->po_box           = $address['POBox'];
            $contactAddress->extended_address = $address['ExtendedAddress'];
            $contactAddress->street_address   = $address['StreetAddress'];
            $contactAddress->locality         = $address['Locality'];
            $contactAddress->region           = $address['Region'];
            $contactAddress->postal_code      = $address['PostalCode'];
            $contactAddress->country          = $address['Country'];
            $contactAddress->save();
            if (isset($address['Type'])) {
                foreach ($address['Type'] as $type) {
                    $addressType = ContactAddressType::where('type', $type)->first();
                    if (null === $addressType) {
                        $addressType       = new ContactAddressType;
                        $addressType->type = $type;
                        $addressType->save();
                    }
                    $contactAddress->contactAddressTypes()->save($addressType);
                }
            }
        }


        // groups
        $card->contactGroups()->sync([]);
        if (isset($parsed->categories[0])) {
            foreach ($parsed->categories[0] as $groupName) {
                $group = ContactGroup::where('group', $groupName)->first();
                if (null === $group) {
                    $group        = new ContactGroup;
                    $group->group = $groupName;
                    $group->save();
                }
                $group->cards()->save($card);
            }
        }


        $card->save();

        return $card;
    }
}