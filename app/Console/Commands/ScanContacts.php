<?php

namespace ContactCleaner\Console\Commands;

use ContactCleaner\Contact\Fixer\BasicFixer;
use ContactCleaner\Contact\Fixer\BasicGlobalFixer;
use ContactCleaner\Models\Card;
use Illuminate\Console\Command;

/**
 *
 */
class ScanContacts extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs all local contacts, and scans and shows problems.';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc:scan';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notFixable = [];
        $fixable    = [];
        $this->line('Going to fix contacts.');
        $fixers       = config('cc.fixers');
        $globalFixers = config('cc.global_fixers');
        $cards        = Card::get();
        /** @var Card $card */
        foreach ($cards as $card) {
            $this->line(sprintf('Now working on "%s".', $card->getFullName()));

            foreach ($fixers as $fixerClass) {
                /** @var BasicFixer $fixerObj */
                $fixerObj = new $fixerClass;
                $fixerObj->setCard($card);
                $fixerObj->scanOnly();

                // collect problems:
                foreach ($fixerObj->notFixable as $message) {
                    $notFixable[] = $message;
                }
                foreach ($fixerObj->fixable as $message) {
                    $this->info($message);
                    $fixable[] = $message;
                }
            }
        }
        $this->line('Now working on global fixers');
        foreach ($globalFixers as $globalFixer) {
            /** @var BasicGlobalFixer $fixerObj */
            $fixerObj = new $globalFixer;
            $fixerObj->setCards($cards);
            $fixerObj->scanOnly();

            // collect problems:
            foreach ($fixerObj->notFixable as $message) {
                $notFixable[] = $message;
            }
            foreach ($fixerObj->fixable as $message) {
                $this->info($message);
                $fixable[] = $message;
            }
        }


        $this->info('Done');
        foreach ($fixable as $message) {
            $this->info($message);
        }
        foreach ($notFixable as $message) {
            $this->error($message);
        }

        return 0;
    }
}
