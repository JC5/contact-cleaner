<?php

namespace ContactCleaner\Console\Commands;

use ContactCleaner\Contact\Fixer\BasicFixer;
use ContactCleaner\Models\Card;
use Illuminate\Console\Command;

class FixContacts extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grabs all local contacts, and tries to fix them.';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc:fix';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $notFixable = [];
        $fixed      = [];
        $this->line('Going to fix contacts.');
        $fixers = config('cc.fixers');
        $fixes  = 0;
        $cards  = Card::get();
        /** @var Card $card */
        foreach ($cards as $card) {
            $this->line(sprintf('Now working on "%s".', $card->getFullName()));

            foreach ($fixers as $fixerClass) {
                /** @var BasicFixer $fixerObj */
                $fixerObj = new $fixerClass;
                $fixerObj->setCard($card);
                $fixerObj->scanAndFix();

                // collect problems:
                foreach ($fixerObj->notFixable as $message) {
                    $notFixable[] = $message;
                }
                foreach ($fixerObj->fixed as $message) {
                    $this->info($message);
                    $fixes++;
                    $fixed[] = $message;
                }
            }
            if ($fixes > 0) {
                //break;
            }
        }
        $this->info('Done');
        foreach ($fixed as $message) {
            $this->info($message);
        }
        foreach ($notFixable as $message) {
            $this->error($message);
        }

        return 0;
    }
}
