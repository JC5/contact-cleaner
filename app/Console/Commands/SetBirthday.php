<?php

namespace ContactCleaner\Console\Commands;

use Artisan;
use Carbon\Carbon;
use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactName;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class SetBirthday extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set contact birthday.';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc:birthday {name} {date}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');
        $date = $this->argument('date');
        // search for user with this name:
        /** @var Builder $contactName */
        $contactNames = ContactName::where(
            function (Builder $q) use ($name) {
                $q->where('full_name', 'LIKE', '%' . $name . '%');
                $q->orWhere('first_name', 'LIKE', '%' . $name . '%');
                $q->orWhere('last_name', 'LIKE', '%' . $name . '%');
            }
        )->get();
        if (0 === $contactNames->count()) {
            $this->error('Found no card');

            return 1;
        }
        if ($contactNames->count() > 1) {
            $this->error(sprintf('Not unique, found %d cards', $contactNames->count()));

            return 1;
        }
        $contactName = $contactNames->first();
        /** @var Card $card */
        $card = $contactName->card;
        $card->refresh();

        if (5 === strlen($date)) {
            $date = '1900-' . $date;
        }

        $this->line(sprintf('Set birthday for %s to %s', $card->getFullName(), $date));
        $carbon             = Carbon::parse($date);
        $card->birthday     = $carbon;
        $card->local_update = true;
        $card->save();
        Artisan::call('cc:push', ['name' => $name]);
        $this->line(sprintf('Pushed %s to server', $card->getFullName()));

        return 0;
    }
}
