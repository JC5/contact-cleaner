<?php

namespace ContactCleaner\Console\Commands;

use ContactCleaner\Models\ContactName;
use ContactCleaner\Support\CardSync;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class ParseCard extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc:parse {name}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('name');

        // search for user with this name:
        /** @var Builder $contactName */
        $contactName = ContactName::where(
            function (Builder $q) use ($name) {
                $q->where('full_name', 'LIKE', '%' . $name . '%');
                $q->orWhere('first_name', 'LIKE', '%' . $name . '%');
                $q->orWhere('last_name', 'LIKE', '%' . $name . '%');
            }
        )->first();
        if (null === $contactName) {
            $this->error('Found no card');

            return 1;
        }
        $card = $contactName->card;
        $card->refresh();
        // step 1. Generate vCard and upload it:
        $syncTool = new CardSync;
        $vCardText = $syncTool->generateVCard($card);
        echo $vCardText;
    }
}
