<?php

namespace ContactCleaner\Console\Commands;

use ContactCleaner\CardDAV\CardDAV;
use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactName;
use ContactCleaner\Support\CardSync;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 *
 * Class PushContacts
 */
class PushContacts extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload changed contacts to contact manager.';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc:push {name?}';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $this->line('Going to push contacts to cloud.');
        // connect to card dav server
        $service = CardDAV::getInstance(env('CARDDAV_URL'), env('CARDDAV_USER'), env('CARDDAV_PASS'));

        // filter by name:
        $name = $this->argument('name');
        if (null === $name) {
            $cards = Card::where('local_update', true)->get();
        }
        if (null !== $name) {
            // search for user with this name:
            /** @var Collection $contactNames */
            $contactNames = ContactName::where(
                function (Builder $q) use ($name) {
                    $q->where('full_name', 'LIKE', '%' . $name . '%');
                    $q->orWhere('first_name', 'LIKE', '%' . $name . '%');
                    $q->orWhere('last_name', 'LIKE', '%' . $name . '%');
                }
            )->get();
            if (1 === $contactNames->count()) {
                /** @var Card $card */
                $card = $contactNames->first()->card;
                $card->refresh();
                $cards = new Collection([$card]);
                $this->line(sprintf('Going to push "%s"', $card->getFullName()));
            } else {
                $this->error('Name is not unique.');
                return 1;
            }
        }


        // three because there are three steps: upload, download (2x) and parse.
        $count    = $cards->count() * 4;
        $bar      = $this->output->createProgressBar($count);
        $syncTool = new CardSync;

        $this->line(sprintf('Going to push %d contact(s).', $cards->count()));

        /** @var Card $card */
        foreach ($cards as $index => $card) {

            $fullName = $card->getFullName();

            Log::debug(sprintf('Now going to push %s', $fullName));

            // step 1. Generate vCard and upload it:
            $vCardText = $syncTool->generateVCard($card);
            $service->update($vCardText, $card->uid);

            Log::debug('New vCard text is: ');
            Log::debug($vCardText);


            $bar->advance();

            // step 2. Download vCard from server again:
            $newVCard = $service->get_vcard($card->uid);
            $bar->advance();
            $newVCardXML       = $service->get_xml_vcard($card->uid);
            $newVCardXMLObject = simplexml_load_string($newVCardXML);
            $cardObj           = $newVCardXMLObject->children()[0];

            $bar->advance();

            Log::debug('Downloaded vCard text is: ');
            Log::debug($newVCard);

            // step 3. Parse vcard again.
            $updatedCard = $syncTool->updateDatabase($card, $newVCard, (string)$cardObj->etag);
            $bar->advance();
        }
        $this->line('');
        $this->line('Done.');

        return 0;
    }
}
