<?php

namespace ContactCleaner\Console\Commands;

use Carbon\Carbon;
use ContactCleaner\CardDAV\CardDAV;
use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactName;
use ContactCleaner\Support\CardSync;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Log;

class ImportContacts extends Command
{
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import your contacts';
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cc:import {name?} {--reset : Reset all cards first}';

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle(): int
    {
        if (true === $this->option('reset')) {
            $this->error('All cards reset');
            Card::where('id', '>', 0)->delete();
        }
        // connect to card dav server
        $service = CardDAV::getInstance(env('CARDDAV_URL'), env('CARDDAV_USER'), env('CARDDAV_PASS'));

        $name = $this->argument('name');
        if (null !== $name) {
            $this->line(sprintf('Request to import person "%s"', $name));

            $contactNames = ContactName::where(
                function (Builder $q) use ($name) {
                    $q->where('full_name', 'LIKE', '%' . $name . '%');
                    $q->orWhere('first_name', 'LIKE', '%' . $name . '%');
                    $q->orWhere('last_name', 'LIKE', '%' . $name . '%');
                }
            )->get();
            if ($contactNames->count() !== 1) {
                $this->error(sprintf('"%s" does not give a result', $name));

                return 1;
            }
            /** @var ContactName $contactName */
            $contactName = $contactNames->first();
            /** @var Card $card */
            $card  =$contactName->card;
            Log::debug(sprintf('Will download and reset %s', $card->getFullName()));
            $vCardString = $service->get_vcard($card->uid);
            Log::debug(sprintf("Imported string is: \n%s", $vCardString));
            $cardSyncer = new CardSync();
            $card = $cardSyncer->updateDatabase($card, $vCardString, 'unknown');
            $this->line('Done!');
            return 0;
        }


        /**
         * First action is to get a list of all UID's on the server,
         * without any CARDDAV data.
         */
        $this->line('Now downloading cards. This will take some time..');
        $result = $service->get(false, false);
        $object = simplexml_load_string($result);

        // sync tool:
        $cardSyncer = new CardSync;


        // loop objects.
        $loopIndex = 0;
        $echoDot   = false;
        foreach ($object->children() as $child) {
            // create or update the database card.
            $uid     = (string)$child->id;
            $card    = Card::where('uid', $uid)->first();
            $newCard = false;
            if (null === $card) {
                $lastModified        = Carbon::createFromFormat('D, d M Y H:i:s T', (string)$child->last_modified);
                $card                = new Card;
                $newCard             = true;
                $card->uid           = $uid;
                $card->local_update  = false;
                $card->etag          = (string)$child->etag;
                $card->last_modified = $lastModified;
            }
            $fullName = '(no name)';
            /** @var ContactName $name */
            $name = $card->contactNames->first();
            if (null !== $name) {
                $fullName = $name->full_name;
            }
            if (true === $newCard) {
                echo '.';
                $echoDot = true;
            }

            // if new card, or no match on eTag, or local update, download it.
            if (true === $newCard || $card->etag !== (string)$child->etag || true === $card->local_update) {
                Log::debug(sprintf('Will download and reset %s', $fullName));
                $vCardString = $service->get_vcard($card->uid);
                Log::debug(sprintf("Imported string is: \n%s", $vCardString));
                $card = $cardSyncer->updateDatabase($card, $vCardString, $child->etag);
            }

            // if not a new card but etag has changed, just mention it:
            if (false === $newCard && $card->etag !== (string)$child->etag) {
                if (true === $echoDot) {
                    $this->line('');
                    $echoDot = false;
                }
                $this->line(sprintf('Downloading updates for "%s" because %s !== %s', $fullName, $card->etag, (string)$child->etag));
            }

            if (false === $newCard && $card->etag === (string)$child->etag) {
                Log::debug(sprintf('Card for "%s" is up-to-date.', $fullName));
            }
        }
        $this->line('');
        $this->line('Done!');

        return 0;
    }
}
