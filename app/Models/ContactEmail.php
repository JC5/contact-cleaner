<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * ContactCleaner\Models\ContactEmail
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $card_id
 * @property string $email
 * @property-read \ContactCleaner\Models\Card $card
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactEmailType[] $contactEmailTypes
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmail whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmail whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmail whereUpdatedAt($value)
 */
class ContactEmail extends Model
{
    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }
    /**
     * @return BelongsToMany
     */
    public function contactEmailTypes(): BelongsToMany
    {
        return $this->belongsToMany(ContactEmailType::class,'email_types');
    }

}