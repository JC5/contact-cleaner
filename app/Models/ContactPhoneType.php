<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ContactPhoneType
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactPhone[] $contactPhones
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhoneType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhoneType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhoneType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhoneType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactPhoneType extends Model
{

    /**
     * @return BelongsToMany
     */
    public function contactPhones(): BelongsToMany
    {
        return $this->belongsToMany(ContactPhone::class);
    }
}