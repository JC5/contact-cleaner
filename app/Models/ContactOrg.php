<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * ContactCleaner\Models\ContactOrg
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $card_id
 * @property-read \ContactCleaner\Models\Card $card
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereUpdatedAt($value)
 * @property string $name
 * @property string|null $unit1
 * @property string|null $unit2
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereUnit1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactOrg whereUnit2($value)
 */
class ContactOrg extends Model
{
    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }
}