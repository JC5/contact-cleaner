<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ContactEvent
 *
 * @property \Illuminate\Support\Carbon|null $date
 * @property string                          $label
 * @property int                             $card_id
 */
class ContactEvent extends Model
{
    protected $casts
                        = [
            'date' => 'date',
        ];
    protected $fillable = ['date', 'label', 'card_id'];

    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }
}