<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * ContactCleaner\Models\ContactAddress
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $card_id
 * @property string $po_box
 * @property string $postal_code
 * @property string $province
 * @property string $country
 * @property-read \ContactCleaner\Models\Card $card
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress wherePoBox($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress wherePostalCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereProvince($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $extended_address
 * @property string $street_address
 * @property string $locality
 * @property string $region
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereExtendedAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereLocality($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereRegion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddress whereStreetAddress($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactAddressType[] $contactAddressTypes
 */
class ContactAddress extends Model
{
    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }

    /**
     * @return BelongsToMany
     */
    public function contactAddressTypes(): BelongsToMany
    {
        return $this->belongsToMany(ContactAddressType::class,'address_types');
    }

}