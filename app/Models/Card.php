<?php

namespace ContactCleaner\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Class Card
 *
 * @mixin \Eloquent
 * @property int                                                                                   $id
 * @property \Illuminate\Support\Carbon|null                                                       $created_at
 * @property \Illuminate\Support\Carbon|null                                                       $updated_at
 * @property string                                                                                $uid
 * @property string                                                                                $version
 * @property string                                                                                $notes
 * @property string                                                                                $photo_type
 * @property string                                                                                $photo
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereNotes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card wherePhoto($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card wherePhotoType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereVersion($value)
 * @property string                                                                                $last_modified
 * @property string                                                                                $etag
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereEtag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereLastModified($value)
 * @property string                                                                                $full_name
 * @property string                                                                                $last_name
 * @property string                                                                                $first_name
 * @property string                                                                                $additional_names
 * @property string                                                                                $prefixes
 * @property string                                                                                $suffixes
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereAdditionalNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card wherePrefixes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereSuffixes($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactName[]    $contactNames
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactEmail[]   $contactEmails
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactOrg[]     $contactOrgs
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactPhone[]   $contactPhones
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactPhoto[]   $contactPhotos
 * @property string|null                                                                           $dod
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereDod($value)
 * @property string|null                                                                           $photo_sha2
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactAddress[] $contactAddresses
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card wherePhotoSha2($value)
 * @property \Illuminate\Support\Carbon|null                                                       $anniversary
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereAnniversary($value)
 * @property \Illuminate\Support\Carbon|null                                                       $birthday
 * @property string                                                                                $nickname
 * @property string                                                                                $photo_uri
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereNickname($value)
 * @property boolean                                                                               $local_update
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactGroup[]   $contactGroups
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\Card whereLocalUpdate($value)
 */
class Card extends Model
{
    protected $casts
        = [
            'last_modified' => 'datetime',
            'dod'           => 'date',
            'anniversary'   => 'date',
            'birthday'      => 'date',
            'local_update'  => 'boolean',
        ];

    /**
     * @return HasMany
     */
    public function contactAddresses(): HasMany
    {
        return $this->hasMany(ContactAddress::class);
    }

    /**
     * @return HasMany
     */
    public function contactEmails(): HasMany
    {
        return $this->hasMany(ContactEmail::class);
    }

    /**
     * @return BelongsToMany
     */
    public function contactGroups(): BelongsToMany
    {
        return $this->belongsToMany(ContactGroup::class, 'cards_groups');
    }

    /**
     * @return HasMany
     */
    public function contactNames(): HasMany
    {
        return $this->hasMany(ContactName::class);
    }
    /**
     * @return HasMany
     */
    public function contactEvents(): HasMany
    {
        return $this->hasMany(ContactEvent::class);
    }

    /**
     * @return HasMany
     */
    public function contactOrgs(): HasMany
    {
        return $this->hasMany(ContactOrg::class);
    }

    /**
     * @return HasMany
     */
    public function contactPhones(): HasMany
    {
        return $this->hasMany(ContactPhone::class);
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        $fullName = '(no name)';
        /** @var ContactName $name */
        $name = $this->contactNames->first();
        if (null !== $name) {
            $fullName = $name->full_name;
        }

        return $fullName;
    }


}
