<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ContactEmailType
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactEmail[] $contactEmails
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmailType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmailType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmailType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactEmailType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactEmailType extends Model
{
    /**
     * @return BelongsToMany
     */
    public function contactEmails(): BelongsToMany
    {
        return $this->belongsToMany(ContactEmail::class);
    }

}