<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * ContactCleaner\Models\ContactAddressType
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactAddress[] $contactAddresses
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddressType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddressType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddressType whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactAddressType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactAddressType extends Model
{
    /**
     * @return BelongsToMany
     */
    public function contactAddresses(): BelongsToMany
    {
        return $this->belongsToMany(ContactAddress::class);
    }

}