<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * ContactCleaner\Models\ContactPhone
 *
 * @mixin \Eloquent
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $card_id
 * @property string $number
 * @property-read \ContactCleaner\Models\Card $card
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\ContactPhoneType[] $contactPhoneTypes
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhone whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhone whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhone whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhone whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactPhone whereUpdatedAt($value)
 */
class ContactPhone extends Model
{
    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }

    /**
     * @return BelongsToMany
     */
    public function contactPhoneTypes(): BelongsToMany
    {
        return $this->belongsToMany(ContactPhoneType::class,'phone_types');
    }
}