<?php
declare(strict_types=1);

namespace ContactCleaner\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class ContactGroup
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $group
 * @property-read \Illuminate\Database\Eloquent\Collection|\ContactCleaner\Models\Card[] $cards
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactGroup whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactGroup extends Model
{
    protected $fillable = ['group'];
    /**
     * @return BelongsToMany
     */
    public function cards(): BelongsToMany
    {
        return $this->belongsToMany(Card::class,'cards_groups');
    }
}