<?php
declare(strict_types=1);

namespace ContactCleaner\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Class ContactName
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int $card_id
 * @property string|null $full_name
 * @property string|null $last_name
 * @property string|null $first_name
 * @property string|null $additional_names
 * @property string|null $prefixes
 * @property string|null $suffixes
 * @property-read \ContactCleaner\Models\Card $card
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereAdditionalNames($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereFullName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName wherePrefixes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereSuffixes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\ContactCleaner\Models\ContactName whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ContactName extends Model
{
    public function card(): BelongsTo
    {
        return $this->belongsTo(Card::class);
    }

}