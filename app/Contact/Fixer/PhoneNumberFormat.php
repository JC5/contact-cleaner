<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactPhone;

class PhoneNumberFormat extends BasicFixer
{
    /** @var Card */
    private $card;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $name    = $this->card->getFullName();
        $numbers = $this->card->contactPhones;

        /** @var ContactPhone $phoneNumber */
        foreach ($numbers as $phoneNumber) {
            // check ascii characters.
            $number   = $phoneNumber->number;
            $filtered = $this->filter($number);
            if ($filtered !== $number) {
                $phoneNumber->number = $filtered;
                $phoneNumber->save();
                $number                   = $filtered;
                $this->card->local_update = true;
                $this->card->save();
                $this->fixed[] = sprintf('%s\'s phone number "%s" was corrected to "%s".', $name, $number, $filtered);
            }

            // make number international.
            if (10 === \strlen($number) && 0 === strpos($number, '06')) {
                $fixed               = '+316' . substr($number, 2, 8);
                $number              = $fixed;
                $phoneNumber->number = $fixed;
                $phoneNumber->save();
                $this->card->local_update = true;
                $this->card->save();
                $this->fixed[] = sprintf('%s\'s phone number "%s" was corrected to "%s".', $name, $number, $fixed);
            }

            // if number starts with 316 and is 11 chars long we can safely push to +316.
            if (11 === \strlen($number) && 0 === strpos($number, '316')) {
                $fixed = '+' . $number;
                $number          = $fixed;
                $phoneNumber->number = $fixed;
                $phoneNumber->save();
                $this->card->local_update = true;
                $this->card->save();

                $this->fixable[] = sprintf('%s\'s phone number "%s" fixed to "%s".', $name, $number, $fixed);
            }

            // must start with a + and not be a special number.
            if ($number{0} !== '+' && 0 !== strpos($number, '0800') && 0 !== strpos($number, '0900') && !$this->isSpecial($number)) {
                $this->notFixable[] = sprintf('%s\'s phone number "%s" does not start with a +.', $name, $number);
            }
            if (0 === strpos($number, '+31') && 12 !== \strlen($number)) {
                $this->notFixable[] = sprintf('%s\'s phone number "%s" is of the wrong length.', $name, $number);
            }

            // +31? total length must be 11
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $name    = $this->card->getFullName();
        $numbers = $this->card->contactPhones;

        /** @var ContactPhone $phoneNumber */
        foreach ($numbers as $phoneNumber) {
            // check ascii characters.
            $number   = $phoneNumber->number;
            $filtered = $this->filter($number);
            if ($filtered !== $number) {
                $this->fixable[] = sprintf('%s\'s phone number "%s" should be corrected to "%s".', $name, $number, $filtered);
            }

            // make number international.
            if (10 === \strlen($number) && 0 === strpos($number, '06')) {
                $fixed           = '+316' . substr($number, 2, 8);
                $number          = $fixed;
                $this->fixable[] = sprintf('%s\'s phone number "%s" should be corrected to "%s".', $name, $number, $fixed);
            }
            // if number starts with 316 and is 11 chars long we can safely push to +316.
            if (11 === \strlen($number) && 0 === strpos($number, '316')) {
                $fixed = '+' . $number;
                $number          = $fixed;
                $this->fixable[] = sprintf('%s\'s phone number "%s" should be corrected to "%s".', $name, $number, $fixed);
            }

            // must start with a + and not be a special number.
            if ($number{0} !== '+' && 0 !== strpos($number, '0800') && 0 !== strpos($number, '0900') && !$this->isSpecial($number)) {
                $this->notFixable[] = sprintf('%s\'s phone number "%s" does not start with a +.', $name, $number);
            }
            if (0 === strpos($number, '+31') && 12 !== \strlen($number)) {
                $this->notFixable[] = sprintf('%s\'s phone number "%s" is of the wrong length.', $name, $number);
            }

            // +31? total length must be 11
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }

    private function filter(string $number): string
    {
        //            0  1   2   3   4   5   6   7   8   9   + , #
        $list = [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 44, 35];// 0 1 2 3 4 5 6 7 8 9 + ,
        if ($this->isSpecial($number)) {
            return $number;
        }

        $len = \strlen($number);
        $new = '';
        for ($i = 0; $i < $len; $i++) {
            $letter = $number{$i};
            $ascii  = \ord($letter);
            if (\in_array($ascii, $list, true)) {
                $new .= $letter;
            }
        }

        return $new;
    }

    private function isSpecial(string $number): bool
    {
        $accepted = ['112', 'BURGERNET', '1233', '1200'];

        return \in_array($number, $accepted, true);
    }
}