<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;

use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactEmail;
use ContactCleaner\Models\ContactGroup;
use Illuminate\Database\QueryException;
use Log;

/**
 * Class GroupByDomain
 */
class GroupByDomain extends BasicFixer
{

    /** @var Card */
    private $card;
    /** @var array */
    private $expectedDomains;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        if (null === $this->card) {
            return;
        }
        $results  = [];
        $fullName = $this->card->getFullName();

        /** @var ContactEmail $email */
        foreach ($this->card->contactEmails as $email) {
            $address = $email->email;
            $parts   = explode('@', $address);
            $domain  = $parts[1] ?? 'empty';
            Log::debug(sprintf('Now scanning address %s', $address));

            // expected domains:
            foreach ($this->expectedDomains as $expectedDomain => $expectedGroup) {
                $len = \strlen($expectedDomain);
                if (substr($domain, -$len) === $expectedDomain) {

                    // is expected to be in group $group.
                    $isInGroup = false;
                    /** @var ContactGroup $group */
                    foreach ($this->card->contactGroups as $contactGroup) {
                        if ($contactGroup->group === $expectedGroup) {
                            $isInGroup = true;
                        }
                    }
                    if (false === $isInGroup) {
                        $dbGroup = ContactGroup::where('group', $expectedGroup)->first();
                        if (null === $dbGroup) {
                            $dbGroup        = new ContactGroup;
                            $dbGroup->group = $expectedGroup;
                            $dbGroup->save();
                        }
                        try {
                            $this->card->contactGroups()->save($dbGroup);
                            $this->card->local_update = true;
                            $this->card->save();
                        } catch (QueryException $e) {
                            $message = sprintf('Tried to give user %s (#%d) group %s but failed.', $fullName, $this->card->id, $expectedGroup);
                            Log::error($message);
                            $this->notFixable[] = $message;
                            continue;
                        }
                        $message       = sprintf('%s has been added to group %s based on email address %s.', $fullName, $expectedGroup, $address);
                        $this->fixed[] = $message;
                    }
                    if (true === $isInGroup) {
                        $message = sprintf('%s is in expected group %s', $fullName, $expectedGroup);
                        Log::debug($message);
                    }
                }
            }
        }
        Log::debug(sprintf('Return array with %d problems.', \count($results)));
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        if (null === $this->card) {
            return;
        }
        $results  = [];
        $fullName = $this->card->getFullName();

        /** @var ContactEmail $email */
        foreach ($this->card->contactEmails as $email) {
            $address = $email->email;
            $parts   = explode('@', $address);
            $domain  = $parts[1] ?? 'empty';
            Log::debug(sprintf('Now scanning address %s', $address));

            // expected domains:
            foreach ($this->expectedDomains as $expectedDomain => $expectedGroup) {
                $len = \strlen($expectedDomain);
                if (substr($domain, -$len) === $expectedDomain) {

                    // is expected to be in group $group.
                    $isInGroup = false;
                    /** @var ContactGroup $group */
                    foreach ($this->card->contactGroups as $contactGroup) {
                        if ($contactGroup->group === $expectedGroup) {
                            $isInGroup = true;
                        }
                    }
                    if (false === $isInGroup) {
                        $message         = sprintf('%s should be added to group %s based on email address %s.', $fullName, $expectedGroup, $address);
                        $this->fixable[] = $message;
                    }
                }
            }
        }
        Log::debug(sprintf('Return array with %d problems.', \count($results)));
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     *
     * @return mixed
     */
    public function setCard(Card $card): void
    {
        $this->card            = $card;
        $this->expectedDomains = config('cc.domains_and_groups');
    }
}