<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactPhone;
use ContactCleaner\Models\ContactPhoneType;
use Illuminate\Database\QueryException;

class BadPhoneType extends BasicFixer
{
    /** @var Card */
    private $card;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $name    = $this->card->getFullName();
        $numbers = $this->card->contactPhones;
        $allowed = ['internet', 'work', 'pref', 'home', 'cell', 'main', 'voice'];
        $replace = [
            'work"'     => 'work',
            '"work'     => 'work',
            'voice"'    => 'home',
            'pref'      => 'voice',
            '"internet' => 'internet',
            '"home'     => 'home',
        ];
        /** @var ContactPhone $phoneNumber */
        foreach ($numbers as $phoneNumber) {
            // check ascii characters.
            $number = $phoneNumber->number;
            $types  = $phoneNumber->contactPhoneTypes;
            /** @var ContactPhoneType $phoneType */
            foreach ($types as $phoneType) {
                if (!\in_array($phoneType->type, $allowed, true)) {
                    $replaced = $replace[$phoneType->type] ?? 'home';

                    // disconnect phone type from phone number?
                    $phoneNumber->contactPhoneTypes()->detach([$phoneType->id]);
                    // add new (correct) phone type for number:
                    $newType = ContactPhoneType::where('type', $replaced)->first();
                    if (null !== $newType) {
                        try {
                            $phoneNumber->contactPhoneTypes()->attach([$newType->id]);
                        } catch (QueryException $exception) {
                            // delete it instead.
                        }
                        $this->fixed[]            = sprintf(
                            'Type [%s] for phone number %s (of %s) is set to %s.', $phoneType->type, $number, $name, $replaced
                        );
                        $this->card->local_update = true;
                        $this->card->save();
                    } else {
                        $this->notFixable[] = sprintf(
                            'Could not fix type [%s] for phone number %s (of %s) to %s.', $phoneType->type, $number, $name, $replaced
                        );
                    }
                }
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $name    = $this->card->getFullName();
        $numbers = $this->card->contactPhones;
        $allowed = ['internet', 'work', 'pref', 'home', 'cell', 'main', 'voice'];
        $replace = [
            'work"'     => 'work',
            '"work'     => 'work',
            'voice"'    => 'home',
            '"internet' => 'internet',
            '"home'     => 'home',
        ];
        /** @var ContactPhone $phoneNumber */
        foreach ($numbers as $phoneNumber) {
            // check ascii characters.
            $number = $phoneNumber->number;
            $types  = $phoneNumber->contactPhoneTypes;
            /** @var ContactPhoneType $phoneType */
            foreach ($types as $phoneType) {
                if (!\in_array($phoneType->type, $allowed, true)) {
                    $replaced        = $replace[$phoneType->type] ?? 'home';
                    $this->fixable[] = sprintf(
                        'Type [%s] for phone number %s (of %s) is not allowed and should be %s instead.', $phoneType->type, $number, $name, $replaced
                    );
                }
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }
}