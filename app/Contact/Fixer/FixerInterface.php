<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;

/**
 * Interface FixerInterface. Scans a contact for specific problems, and fix them as well.
 */
interface FixerInterface
{
    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void;

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void;
}