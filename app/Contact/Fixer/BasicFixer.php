<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;

/**
 * Class BasicFixer
 */
abstract class BasicFixer implements FixerInterface
{
    /** @var array */
    public $fixable;
    /** @var array */
    public $fixed;
    /** @var array */
    public $notFixable;

    public function __construct()
    {
        $this->fixed      = [];
        $this->fixable    = [];
        $this->notFixable = [];
    }

    /**
     * Scan the card, and fix if possible.
     */
    abstract public function scanAndFix(): void;

    /**
     * Scan the card, do not fix anything.
     */
    abstract public function scanOnly(): void;

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    abstract public function setCard(Card $card): void;
}