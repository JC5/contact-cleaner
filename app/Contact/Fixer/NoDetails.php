<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use Log;

/**
 * Class NoDetails
 */
class NoDetails extends BasicFixer
{
    /** @var Card */
    private $card;

    /**
     * Scan the card, and fix if possible.
     *
     * No notes. No organisation.
     */
    public function scanAndFix(): void
    {
        $notes  = \strlen((string)$this->card->notes);
        $orgs   = $this->card->contactOrgs->count();
        $groups = $this->card->contactGroups->count();

        Log::debug(sprintf('%s note length is %d, org count is %d and group count %d', $this->card->getFullName(), $notes, $orgs, $groups));

        if (0 === $notes && 0 === $groups && 0 === $orgs && null === $this->card->birthday && null === $this->card->dod) {
            $this->notFixable[] = sprintf('%s has no details except contact details.', $this->card->getFullName());
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $this->scanAndFix();
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }
}