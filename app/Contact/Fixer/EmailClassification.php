<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactEmail;
use ContactCleaner\Models\ContactEmailType;
use Log;

class EmailClassification extends BasicFixer
{
    /** @var Card */
    private $card;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $emails          = $this->card->contactEmails;
        $name            = $this->card->getFullName();
        $classifications = config('cc.email_classification');
        /** @var ContactEmail $email */
        foreach ($emails as $email) {
            $address = $email->email;
            $parts   = explode('@', $address);
            $domain  = $parts[1] ?? 'empty';
            foreach ($classifications as $searchDomain => $classification) {
                $len = \strlen($searchDomain);
                if (substr($domain, -$len) === $searchDomain) {
                    Log::debug(sprintf('Found %s in %s', $address, $searchDomain));
                    $result = $this->ofClassification($email, $classification);
                    if (!$result) {
                        $res = $this->setClassification($email, $classification);
                        if (true === $res) {
                            $this->card->local_update = true;
                            $this->card->save();
                            $this->fixed[] = sprintf('Set classification of email address "%s" to "%s"', $address, $classification);
                        }
                        if (false === $res) {
                            $this->notFixable[] = sprintf(
                                'Tried to set classification of email address "%s" of "%s" to "%s" but failed!', $address, $name, $classification
                            );
                        }
                    }
                }
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $emails          = $this->card->contactEmails;
        $name            = $this->card->getFullName();
        $classifications = config('cc.email_classification');
        /** @var ContactEmail $email */
        foreach ($emails as $email) {
            $address = $email->email;
            $parts   = explode('@', $address);
            $domain  = $parts[1] ?? 'empty';
            foreach ($classifications as $searchDomain => $classification) {
                $len = \strlen($searchDomain);
                if (substr($domain, -$len) === $searchDomain) {
                    Log::debug(sprintf('Found %s in %s', $address, $searchDomain));
                    $result = $this->ofClassification($email, $classification);
                    if (!$result) {
                        $this->fixable[] = sprintf('Should set classification of email address "%s" to "%s"', $address, $classification);
                    }
                }
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }

    /**
     * @param ContactEmail $email
     * @param string       $classification
     *
     * @return bool
     */
    private function ofClassification(ContactEmail $email, string $classification): bool
    {
        $result = false;
        /** @var ContactEmailType $emailType */
        foreach ($email->contactEmailTypes as $emailType) {
            Log::debug(sprintf('Type is %s', $emailType->type));
            if ($emailType->type === $classification) {

                $result = true;
            }
        }

        return $result;
    }

    /**
     * @param ContactEmail $email
     * @param string       $classification
     *
     * @return bool
     */
    private function setClassification(ContactEmail $email, string $classification): bool
    {
        $internet  = ContactEmailType::where('type', 'internet')->first();
        $thisClass = ContactEmailType::where('type', $classification)->first();
        if (null !== $internet && null !== $thisClass) {
            $email->contactEmailTypes()->sync([$internet->id, $thisClass->id]);

            return true;
        }

        return false;
    }
}