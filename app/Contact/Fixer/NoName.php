<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;

class NoName extends BasicFixer
{
    /** @var Card */
    private $card;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $name = $this->card->getFullName();

        if ('(no name)' === $name) {

            $email = null === $this->card->contactEmails()->first() ? '(no email)' : $this->card->contactEmails()->first()->email;
            $phone = null === $this->card->contactPhones()->first() ? '(no phone)' : $this->card->contactPhones()->first()->number;
            $uid   = $this->card->uid;

            $this->notFixable[] = sprintf('No name for contact. Phone: %s, email: %s, uid: %s', $phone, $email, $uid);
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $this->scanAndFix();
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }
}