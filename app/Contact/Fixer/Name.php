<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactGroup;
use ContactCleaner\Models\ContactName;
use Illuminate\Database\QueryException;
use Log;

/**
 * Class Name
 */
class Name extends BasicFixer
{
    /** @var Card */
    private $card;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        /** @var ContactName $name */
        $name = $this->card->contactNames()->first();


        // do last names:
        $lastNames = config('cc.last_name_classification');
        if (null !== $name) {
            $lastName = $name->last_name;
            if (isset($lastNames[$lastName])) {
                // user must be in group:
                $expectedGroup = $lastNames[$lastName];
                // is expected to be in group $group.
                $isInGroup = false;
                /** @var ContactGroup $group */
                foreach ($this->card->contactGroups as $contactGroup) {
                    if ($contactGroup->group === $expectedGroup) {
                        $isInGroup = true;
                    }
                }
                if (false === $isInGroup) {
                    $dbGroup = ContactGroup::where('group', $expectedGroup)->first();
                    if (null === $dbGroup) {
                        $dbGroup        = new ContactGroup;
                        $dbGroup->group = $expectedGroup;
                        $dbGroup->save();
                    }
                    try {
                        $this->card->contactGroups()->save($dbGroup);
                        $this->card->local_update = true;
                        $this->card->save();
                    } catch (QueryException $e) {
                        $message = sprintf('Tried to give user %s (#%d) group %s but failed.', $this->card->getFullName(), $this->card->id, $expectedGroup);
                        Log::error($message);
                        $this->notFixable[] = $message;

                        return;
                    }
                    $message       = sprintf('%s has been added to group %s based on last name %s.', $this->card->getFullName(), $expectedGroup, $lastName);
                    $this->fixed[] = $message;
                }
            }
        }
        if (null !== $name) {
            // common tussenvoegsels not in last name.
            $fields = ['additional_names', 'prefixes', 'suffixes'];
            foreach ($fields as $field) {
                $current = (string)$name->$field;
                $search  = ['van der ', 'van ', 'te ', 'de '];
                foreach ($search as $entry) {
                    if (false !== strpos($current, $entry)) {
                        $this->notFixable[] = sprintf('%s for user %s should not contain "%s"', $field, $this->card->getFullName(), $entry);
                    }
                }
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        /** @var ContactName $name */
        $name = $this->card->contactNames()->first();
        // do last names:
        $lastNames = config('cc.last_name_classification');
        if (null !== $name) {
            $lastName = $name->last_name;
            if (isset($lastNames[$lastName])) {
                // user must be in group:
                $expectedGroup = $lastNames[$lastName];
                // is expected to be in group $group.
                $isInGroup = false;
                /** @var ContactGroup $group */
                foreach ($this->card->contactGroups as $contactGroup) {
                    if ($contactGroup->group === $expectedGroup) {
                        $isInGroup = true;
                    }
                }
                if (false === $isInGroup) {
                    $message         = sprintf('User "%s" (#%d) should be in group "%s".', $this->card->getFullName(), $this->card->id, $expectedGroup);
                    $this->fixable[] = $message;
                }
            }
        }
        if (null !== $name) {
            // common tussenvoegsels not in last name.
            $fields = ['additional_names', 'prefixes', 'suffixes'];
            foreach ($fields as $field) {
                $current = (string)$name->$field;
                $search  = ['van der ', 'van ', 'te ', 'de '];
                foreach ($search as $entry) {
                    if (false !== strpos($current, $entry)) {
                        $this->notFixable[] = sprintf('%s for user %s should not contain "%s"', $field, $this->card->getFullName(), $entry);
                    }
                }
            }
        }
        if (null === $name) {
            $this->notFixable[] = sprintf('Contact "%s" (#%d) does not have a name', $this->card->uid, $this->card->id);
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }
}