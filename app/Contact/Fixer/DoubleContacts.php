<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use Illuminate\Support\Collection;

/**
 *
 * Class DoubleContacts
 */
class DoubleContacts extends BasicGlobalFixer
{

    /** @var Collection */
    private $cards;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $names = [];
        /** @var Card $card */
        foreach ($this->cards as $card) {
            $fullName           = $card->getFullName();
            $names[$fullName][] = $card->uid;
        }
        /**
         * @var string $name
         * @var array  $collected
         */
        foreach ($names as $name => $collected) {
            if (count($collected) > 1) {
                $uids               = implode(', ', $collected);
                $this->notFixable[] = sprintf('%s is a double contact: %s', $name, $uids);
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $names = [];
        /** @var Card $card */
        foreach ($this->cards as $card) {
            $fullName           = $card->getFullName();
            $names[$fullName][] = $card->uid;
        }
        /**
         * @var string $name
         * @var array  $collected
         */
        foreach ($names as $name => $collected) {
            if (count($collected) > 1) {
                $uids               = implode(', ', $collected);
                $this->notFixable[] = sprintf('%s is a double contact: %s', $name, $uids);
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Collection $cards
     */
    public function setCards(Collection $cards): void
    {
        $this->cards = $cards;
    }
}