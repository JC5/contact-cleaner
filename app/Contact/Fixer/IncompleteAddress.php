<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactAddress;

/**
 *
 * Class IncompleteAddress
 */
class IncompleteAddress extends BasicFixer
{
    private $bad;
    /** @var Card */
    private $card;

    public function __construct()
    {
        parent::__construct();

        // some bad addresses:
        $this->bad = [
            // only "Netherlands" as the address.
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => '',
                'postal_code'      => '',
                'locality'         => '',
                'region'           => '',
                'country'          => 'Netherlands',
            ],
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => '',
                'postal_code'      => '',
                'locality'         => '',
                'region'           => '',
                'country'          => 'Nederland',
            ],
            // only "NL" as the address.
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => '',
                'postal_code'      => '',
                'locality'         => '',
                'region'           => '',
                'country'          => 'NL',
            ],
            // Rabobank datacenter in Boxtel.
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Bloemmolen 2',
                'postal_code'      => '5281 PH',
                'locality'         => 'Boxtel',
                'region'           => 'Noord Brabant',
                'country'          => 'Nederland',
            ],

            // Rabobank head quarters
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Croeselaan 18',
                'postal_code'      => '3521 CB',
                'locality'         => 'Utrecht',
                'region'           => 'Utrecht',
                'country'          => 'Nederland',
            ],
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Croeselaan 28',
                'postal_code'      => '3521 CB',
                'locality'         => 'Utrecht',
                'region'           => 'Utrecht',
                'country'          => 'Nederland',
            ],
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Croeselaan 18',
                'postal_code'      => '3521 CB',
                'locality'         => 'Utrecht',
                'region'           => 'Utrecht',
                'country'          => 'NL',
            ],
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Croeselaan 18',
                'postal_code'      => '3521 CB',
                'locality'         => 'Utrecht',
                'region'           => 'Utrecht',
                'country'          => '',
            ],


            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Laan van Eikenstein 9',
                'postal_code'      => '3705 AR',
                'locality'         => 'Zeist',
                'region'           => 'Utrecht',
                'country'          => '',
            ],
            [
                'po_box'           => '',
                'extended_address' => '',
                'street_address'   => 'Laan van Eikenstein 9',
                'postal_code'      => '3705 AR',
                'locality'         => 'Zeist',
                'region'           => 'Utrecht',
                'country'          => 'Nederland',
            ],

        ];
    }

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $name      = $this->card->getFullName();
        $addresses = $this->card->contactAddresses;
        /** @var ContactAddress $address */
        foreach ($addresses as $address) {
            if ($this->isBadAddress($address)) {
                $this->fixed[] = sprintf('Address %s for %s is incomplete and should be removed.', $this->formatAddress($address), $name);
                $address->delete();
                $this->card->local_update = true;
                $this->card->save();
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $name      = $this->card->getFullName();
        $addresses = $this->card->contactAddresses;
        /** @var ContactAddress $address */
        foreach ($addresses as $address) {
            if ($this->isBadAddress($address)) {
                $this->fixable[] = sprintf('Address %s for %s is incomplete and should be removed.', $this->formatAddress($address), $name);
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }

    private function formatAddress(ContactAddress $address): string
    {
        $array = $address->toArray();
        unset($array['created_at'], $array['updated_at']);

        return sprintf(
            '%s/%s/%s/%s/%s/%s/%s',
            $array['country'],
            $array['region'],
            $array['locality'],
            $array['postal_code'],
            $array['street_address'],
            $array['extended_address'],
            $array['po_box']
        );

    }

    /**
     * @param ContactAddress $address
     *
     * @return bool
     */
    private function isBadAddress(ContactAddress $address): bool
    {
        foreach ($this->bad as $bad) {
            if (
                $bad['po_box'] === $address->po_box
                && $bad['extended_address'] === $address->extended_address
                && $bad['street_address'] === $address->street_address
                && $bad['postal_code'] === $address->postal_code
                && $bad['locality'] === $address->locality
                && $bad['region'] === $address->region
                && $bad['country'] === $address->country
            ) {
                return true;
            }
        }

        return false;
    }
}