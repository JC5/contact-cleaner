<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;

use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactGroup;
use Log;

/**
 *
 * Class PhotoByGroup
 */
class PhotoByGroup extends BasicFixer
{
    /** @var Card */
    private $card;

    /** @var array */
    private $expectedPhotos;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        if (null === $this->card) {
            return;
        }
        $fullName = $this->card->getFullName();

        /** @var ContactGroup $group */
        foreach ($this->card->contactGroups as $group) {
            $groupName = $group->group;

            Log::debug(sprintf('Now verifying group %s', $groupName));
            // expected domains:
            foreach ($this->expectedPhotos as $expectedGroup => $expectedPhoto) {
                if ($expectedGroup === $groupName) {
                    // check photo hash:
                    if (null === $this->card->photo_uri && $this->card->photo_uri !== $expectedPhoto) {
                        $message = sprintf('%s is in group %s and now has a profile photo for group.', $fullName, $expectedGroup);
                        // try to fix it:
                        $this->card->photo_type   = null;
                        $this->card->photo        = null;
                        $this->card->photo_sha2   = null;
                        $this->card->photo_uri    = $expectedPhoto;
                        $this->card->local_update = 1;
                        $this->card->save();

                        // report the problem:
                        Log::debug($message);
                        $this->fixed[] = $message;
                    }

                    if ($this->card->photo_uri === $expectedPhoto) {
                        $message = sprintf('%s is in group %s and has the correct profile photo.', $fullName, $expectedGroup);
                        Log::debug($message);
                    }
                }
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        if (null === $this->card) {
            return;
        }
        $fullName = $this->card->getFullName();

        /** @var ContactGroup $group */
        foreach ($this->card->contactGroups as $group) {
            $groupName = $group->group;

            Log::debug(sprintf('Now verifying group %s', $groupName));
            // expected domains:
            foreach ($this->expectedPhotos as $expectedGroup => $expectedPhoto) {
                if ($expectedGroup === $groupName && null === $this->card->photo_uri && $this->card->photo_uri !== $expectedPhoto) {
                    $message         = sprintf('%s is in group %s and should have a profile photo for group.', $fullName, $expectedGroup);
                    $this->fixable[] = $message;
                }
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     *
     * @return mixed
     */
    public function setCard(Card $card): void
    {
        $this->card           = $card;
        $this->expectedPhotos = config('cc.groups_and_photos');
    }
}