<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;

use ContactCleaner\Models\Card;
use ContactCleaner\Models\ContactEmail;
use Log;

/**
 *
 * Class PhotoByEmailDomain
 */
class PhotoByEmailDomain extends BasicFixer
{
    /** @var Card */
    private $card;

    /** @var array */
    private $expectedPhotos;

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        if (null === $this->card) {
            return;
        }
        $fullName = $this->card->getFullName();

        /** @var ContactEmail $email */
        foreach ($this->card->contactEmails as $email) {
            $address = $email->email;
            $parts   = explode('@', $address);
            $domain  = $parts[1] ?? 'empty';
            Log::debug(sprintf('Now scanning address %s', $address));

            // expected domains:
            foreach ($this->expectedPhotos as $expectedDomain => $expectedPhoto) {
                if ($expectedDomain === $domain) {
                    // check photo hash:
                    if (null === $this->card->photo_uri && $this->card->photo_uri !== $expectedPhoto) {
                        $message = sprintf('%s has email address on %s and now has a profile photo for group.', $fullName, $expectedDomain);
                        // try to fix it:
                        $this->card->photo_type   = null;
                        $this->card->photo        = null;
                        $this->card->photo_sha2   = null;
                        $this->card->photo_uri    = $expectedPhoto;
                        $this->card->local_update = 1;
                        $this->card->save();

                        // report the problem:
                        Log::debug($message);
                        $this->fixed[] = $message;
                    }

                    if ($this->card->photo_uri === $expectedPhoto) {
                        $message = sprintf('%s is in group %s and has the correct profile photo.', $fullName, $expectedDomain);
                        Log::debug($message);
                    }
                }
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        if (null === $this->card) {
            return;
        }
        $fullName = $this->card->getFullName();

        /** @var ContactEmail $email */
        foreach ($this->card->contactEmails as $email) {
            $address = $email->email;
            $parts   = explode('@', $address);
            $domain  = $parts[1] ?? 'empty';
            Log::debug(sprintf('Now scanning address %s', $address));

            // expected domains:
            foreach ($this->expectedPhotos as $expectedDomain => $expectedPhoto) {
                if ($expectedDomain === $domain && null === $this->card->photo_uri && $this->card->photo_uri !== $expectedPhoto) {
                    $message         = sprintf('%s has email address on %s and should have a profile photo for group.', $fullName, $expectedDomain);
                    $this->fixable[] = $message;
                }
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     *
     * @return mixed
     */
    public function setCard(Card $card): void
    {
        $this->card           = $card;
        $this->expectedPhotos = config('cc.domains_and_photos');
    }
}