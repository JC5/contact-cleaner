<?php
declare(strict_types=1);

namespace ContactCleaner\Contact\Fixer;


use ContactCleaner\Models\Card;

class Spaces extends BasicFixer
{
    /** @var Card */
    private $card;

    private $search
        = [
            "\u{0001}", // start of heading
            "\u{0002}", // start of text
            "\u{0003}", // end of text
            "\u{0004}", // end of transmission
            "\u{0005}", // enquiry
            "\u{0006}", // ACK
            "\u{0007}", // BEL
            "\u{0008}", // backspace
            "\u{000E}", // shift out
            "\u{000F}", // shift in
            "\u{0010}", // data link escape
            "\u{0011}", // DC1
            "\u{0012}", // DC2
            "\u{0013}", // DC3
            "\u{0014}", // DC4
            "\u{0015}", // NAK
            "\u{0016}", // SYN
            "\u{0017}", // ETB
            "\u{0018}", // CAN
            "\u{0019}", // EM
            "\u{001A}", // SUB
            "\u{001B}", // escape
            "\u{001C}", // file separator
            "\u{001D}", // group separator
            "\u{001E}", // record separator
            "\u{001F}", // unit separator
            "\u{007F}", // DEL
            "\u{00A0}", // non-breaking space
            "\u{1680}", // ogham space mark
            "\u{180E}", // mongolian vowel separator
            "\u{2000}", // en quad
            "\u{2001}", // em quad
            "\u{2002}", // en space
            "\u{2003}", // em space
            "\u{2004}", // three-per-em space
            "\u{2005}", // four-per-em space
            "\u{2006}", // six-per-em space
            "\u{2007}", // figure space
            "\u{2008}", // punctuation space
            "\u{2009}", // thin space
            "\u{200A}", // hair space
            "\u{200B}", // zero width space
            "\u{202F}", // narrow no-break space
            "\u{3000}", // ideographic space
            "\u{FEFF}", // zero width no -break space
        ];

    /**
     * Scan the card, and fix if possible.
     */
    public function scanAndFix(): void
    {
        $fields     = ['full_name', 'last_name', 'additional_names', 'prefixes', 'suffixes'];
        $nameObject = $this->card->contactNames()->first();
        if (null !== $nameObject) {
            foreach ($fields as $field) {
                $original = (string)$nameObject->$field;
                if ('' !== $original) {
                    // fix name:
                    $corrected = trim($original);
                    $corrected = str_replace($this->search, "\x20", $corrected);
                    $corrected = str_replace("\x20\x20", "\x20", $corrected);

                    if ($corrected !== $original) {
                        $this->fixed[]      = sprintf('"%s" updated to "%s" for %s', $original, $corrected, $this->card->getFullName());
                        $nameObject->$field = $corrected;
                        $nameObject->save();
                        $this->card->local_update = true;
                        $this->card->save();
                    }
                }
            }
        }
    }

    /**
     * Scan the card, do not fix anything.
     */
    public function scanOnly(): void
    {
        $fields     = ['full_name', 'last_name', 'additional_names', 'prefixes', 'suffixes'];
        $nameObject = $this->card->contactNames()->first();
        if (null !== $nameObject) {
            foreach ($fields as $field) {
                $original = (string)$nameObject->$field;
                if ('' !== $original) {
                    // fix name:
                    $corrected = trim($original);
                    $corrected = str_replace($this->search, "\x20", $corrected);
                    $corrected = str_replace("\x20\x20", "\x20", $corrected);

                    if ($corrected !== $original) {
                        $this->fixable[] = sprintf('"%s" should be updated to "%s" for %s', $original, $corrected, $this->card->getFullName());
                    }
                }
            }
        }
    }

    /**
     * Set the card to be scanned.
     *
     * @param Card $card
     */
    public function setCard(Card $card): void
    {
        $this->card = $card;
    }
}